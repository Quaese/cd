/*global define*/

define('app/collections/cdcollection', ['backbone', 'app/models/cd'], function (Backbone, CD) {
    var CDCollection = Backbone.Collection.extend({
        model: CD
    });

    return CDCollection;
});