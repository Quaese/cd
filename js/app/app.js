/*global define*/

define('app/app', [
    'jquery',
    'backbone',
    'app/collections/cdcollection',
    'app/collections/bandcollection',
    'app/views/content',
    'app/views/cdview'
], function ($, Backbone, CDCollection, BandCollection, Content, CDView) {
    "use strict";

    return Backbone.View.extend({
        el: $('body'),

        initialize : function (options) {
            options.bands = options.bands || [];
            options.cds = options.cds || [];

            // setup collections/lists
            this.cdCollection = new CDCollection(options.cds);
            this.bandCollection = new BandCollection(options.bands);

            // View für den Inhalt definieren
            this.content = new Content({
                el: this.$('#content'),
                collection: this.cdCollection,
                bands: this.bandCollection
            });

            // View für den Inhalt definieren
            this.cdview = new CDView({
                el: this.$('#content'),
                collection: this.cdCollection,
                bands: this.bandCollection
            });
        },

        init : function () {
            //this.content.render();
            this.content.generateCDList();
        },

        addCD: function () {
            console.log('add => calls App.addCD');
        },

        showCD : function (id) {
            this.cdview.render(id);
        }
    });
});