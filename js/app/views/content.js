/*global window, define, location*/
/*jslint unparam:true*/

define('app/views/content', [
    'jquery',
    'backbone',
    'mustache',
    'text!app/templates/content.html'
], function ($, Backbone, Mustache, template) {
    "use strict";

    var Content = Backbone.View.extend({
        events: {
            'click button.show-cd': 'showCD',
            'click button.edit-cd': 'editCD',
            'click button.delete-cd': 'deleteCD',
            'click button.sort-cd': 'sortCD'
        },

        initialize: function (options) {
            // this bands enthält bandCollection
            this.bands = options.bands || [];
            this.asc = true;

            // this.collection enthält CDCollection
            this.listenTo(this.collection, 'add change remove', this.generateCDList);

            //this.collection.trigger('change');
        },

        render: function () {
            // Liste nach Bandname (band) aufsteigend sortieren
            this.sortBy({
                asc: this.asc,
                _key: 'band'
            });

            this.template = Mustache.render(template, {
                cds: this.cds
            });

            this.$el.html(this.template);
        },

        // generiere Array mit allen Informationen zu den CDs
        generateCDList: function () {
            var _this = this;

            _this.cds = [];

            _.each(this.collection.models, function (value, key, list) {
                _this.cds.push($.extend({}, value.attributes, _.omit(_this.bands.findWhere({id: value.attributes.band_id}).attributes, 'id')));
            });

            this.render();
        },

        //updateList: function () {},

        // Sortiert die CD-Band-Liste anhand eines Schlüsses auf- oder absteigend
        // @options._key - (string) Schlüssel, nach dem sortiert werden soll (default: band)
        // @options.asc  - (boolean) auf- oder absteigend sortieren (default: true = absteigend)
        sortBy: function (options) {
            // test options parameter (defaults: asc=true, _key=band)
            var asc = (options && options.asc !== undefined) ? options.asc : true,
                _key = (options && options._key !== undefined) ? options._key : 'band';

            // sort array of objects by _key
            this.cds = _.sortBy(this.cds, function (index) {
                return index[_key];
            });

            // revert sorting
            if (!asc) {
                this.cds = this.cds.reverse();
            }
        },

        sortCD: function (evt) {
            this.asc = !this.asc;
            this.render();
        },

        showCD: function (evt) {
            var id = $(evt.currentTarget).data('id');

            // new route
            location.hash = 'cd/' + id;
        },

        deleteCD: function (evt) {
            var id = $(evt.currentTarget).data('id');

            // ToDo: Schönen Dialog zum Bestätigen
            //if(window.confirm(this.cds[index].title + " von " + this.cds[index].band + " wirklich löschen?")){
            if (window.confirm("CD wirklich löschen?")) {
                this.collection.remove(this.collection.get(id));
            }
        }
    });

    return Content;
});