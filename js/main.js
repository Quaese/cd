/*globals requirejs, require*/

// http://www.emanuel-kluge.de/tutorial/backbone-js-tutorial-die-merkliste/
requirejs.config({
    baseUrl: 'js',
    shim: {
        'backbone': {
            //These script dependencies should be loaded before loading
            //backbone.js
            deps: ['underscore', 'jquery']
            // ,
            // //Once loaded, use the global 'Backbone' as the
            // //module value.
            // exports: 'Backbone'
        },
        'localstorage': {
            deps: ['backbone']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'underscore': {
            // exports: '_'
        }
    },
    paths: {
        'jquery': ['libs/jquery/jquery.min'],
        'backbone': ['libs/backbone/backbone'],                         // Dieses Modul hat Abhängigkeiten, die in shim definiert werden müssen
        'bootstrap': ['libs/bootstrap/bootstrap.min'],                  // Dieses Modul hat Abhängigkeiten, die in shim definiert werden müssen
        'domReady': ['libs/requirejs/domReady'],                        // Von: http://requirejs.org/docs/download.html#plugins
        'text': ['libs/requirejs/text'],                                // Von: http://requirejs.org/docs/download.html#plugins (https://github.com/requirejs/text)
        'json': ['libs/requirejs/json'],                                // Von: https://github.com/millermedeiros/requirejs-plugins (Use: 'json!path/to/file.json')
        'localstorage': ['libs/backbone/backbone.localStorage-min'],    // Von: https://github.com/jeromegn/Backbone.localStorage
        'underscore': ['libs/underscore/underscore'],
        'mustache': ['libs/mustache/mustache']                          // Von: https://github.com/janl/mustache.js
    },

    deps: ['backbone', 'app/router', 'domReady!'],
    callback: function (Backbone, Router) {
        var controller = new Router();

        Backbone.history.start();

        return controller;
    }
});