/*global define*/

define('app/collections/bandcollection', ['backbone', 'app/models/band'], function (Backbone, Band) {
    var BandCollection = Backbone.Collection.extend({
        model: Band
    });

    return BandCollection;
});