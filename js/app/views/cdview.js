/*global define*/

define('app/views/cdview', [
    'jquery',
    'backbone',
    'mustache',
    'text!app/templates/cd.html'
], function ($, Backbone, Mustache, template) {
    "use strict";

    var CDView = Backbone.View.extend({
        events: {
        },

        initialize: function (options) {
            this.bands = options.bands || [];

            //this.listenTo(this.collection, 'add change remove', this.render);
        },

        render: function (id) {
            var cd = this.collection.get(id).toJSON(),
                band = this.bands.get(cd.band_id).toJSON();

            this.template = Mustache.render(template, $.extend({}, cd, band));

            this.$el.html(this.template);
        }
    });

    return CDView;
});