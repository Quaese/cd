/*global define*/

define('app/router', [
    'backbone',
    'app/app',

    'localstorage',
    'bootstrap'
], function (Backbone, App) {
    "use strict";

    var bands = [
            {id: 0, band: 'AC/DC', from: 'AUS', genre: 'Hardrock'},
            {id: 1, band: 'Blind Guardian', from: 'GER', genre: 'Metal'},
            {id: 2, band: 'Judas Priest', from: 'UK', genre: 'Metal'},
            {id: 3, band: 'ZZ Top', from: 'USA', genre: 'Rock'}
        ],
        cds = [
            {id: 0, band_id: 0, title: 'Let there be rock'},
            {id: 1, band_id: 1, title: 'Tales From The Twilight World'},
            {id: 2, band_id: 0, title: 'Highway to hell'},
            {id: 3, band_id: 2, title: 'Ram it down'},
            {id: 4, band_id: 3, title: 'Afterburner'}
        ];

    return Backbone.Router.extend({

        routes : {
            '' : 'init',
            'cd/': 'init',
            'cd/:id' : 'showCD',
            'add' : 'addCD'
        },

        initialize : function () {
            this.app = new App({
                bands: bands,
                cds: cds
            });
        },

        init : function () {
            this.app.init();
        },

        addCD: function () {
            console.log('add => calls App.addCD');
        },

        showCD : function (id) {
            this.app.showCD(id);
        }

    });
});