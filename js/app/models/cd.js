/*global define*/

define('app/models/cd', ['backbone'], function (Backbone) {
    var CD = Backbone.Model.extend({
        defaults: {
            id: 0,
            band_id: 0,
            title: 'one',
            year: '1970',
            rating: 5
        }
    });

    return CD;
});