/*global define*/

define('app/models/band', ['backbone'], function (Backbone) {
    var CD = Backbone.Model.extend({
        defaults: {
            id: 0,
            band: 'AC/DC',
            from: 'AUS',
            genre: 'Hardrock'
        }
    });

    return CD;
});